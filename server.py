#!/usr/bin/env python
'''
	Zeta robot protocol between main_board and controller_board.
	Server Program : running on controller_board
	(using Python 2.7)

	History:
		20180529 kyuhsim - created.
'''

import sys
import socket
from SocketServer import ThreadingTCPServer, StreamRequestHandler

### configurations that can be changed
gPort = 9006

### global variables
gSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 	# create socket (SOCK_STREAM means TCP socket) 
gCache = { 'ClientProtocolVersion' : 'Unknown', 
		'ServerProtocolVersion' : '0.1', 
		'ServerIP' : 'localhost',
		'ServerPort' : 9006,
		'ServerStatus' : 'Begin', 
		'ClientStatus' : 'Unknown',
		'MsgNum' : 0,
		'NumPOI' : 3,
		'ListPOI' : [ 'Entrance', 'Front', 'Elevator' ]
	}


def processMsg( conn, msg ):
	msg_list = msg.split()			# make list with space separated msg string
	main_cmd = msg_list[ 0 ]		# first element is the main command
	main_cmd = main_cmd.lower()		# make lower case
	msg_num = msg_list[ 1 ]			# second element is the message sequence number

	if ( main_cmd == 'ping' ):
		conn.send( 'PONG ' + msg_num )
	elif (main_cmd == 'pong' ):
		conn.send( 'PING ' + msg_num )
	elif (main_cmd == ':/quit' ):
		print '! quit mesg received'
		conn.close()
	elif (main_cmd == 'getstatus' ):
		conn.send( 'OK ' + msg_num + " for mesg: " + msg )
	elif (main_cmd == 'getnumpoi' ):
		conn.send( 'OK ' + msg_num + " " + str( gCache[ 'NumPOI' ] ) )
	elif (main_cmd == 'getpoi' ):
		return_str = " ".join( gCache[ 'ListPOI' ] )	# list to string with spaces
		conn.send( 'OK ' + msg_num + " " + return_str )
	elif (main_cmd == 'moveto' ):
		if ( len( msg_list ) < 3 ):						# command, message number, POI number
			conn.send( 'FAIL ' + msg_num + ' MoveTo command need POI number' )
		else:
			destination = msg_list[ 2 ]					# third element of MoveTo command is destination POI number (in string format)
			#####---- should insert robot movement calling code HERE -----#####
			conn.send( 'OK ' + msg_num + ' Moved to POI' + destination )
	elif (main_cmd == 'rotate' ):
		if ( len( msg_list ) < 4 ):
			conn.send( 'FAIL ' + msg_num + ' Rotate command need direction and degree' )
		else:
			direction = msg_list[ 2 ]					# third element of Rotate command is direction (left or right)
			if (direction != 'left' and direction != 'right'):
				conn.send( 'FAIL ' + msg_num + ' Rotate should include left or right direction' )		
			else:
				degree = msg_list[ 3 ]					# 4th element of Rotate command is degree (in string format)
				#####---- should insert robot movement calling code HERE -----#####
				conn.send( 'OK ' + msg_num + ' Rotated ' + direction + ' with ' + degree + ' degree' )
	elif (main_cmd == 'ledon' ):
		#####---- should insert Led ON calling code HERE -----#####
		conn.send( 'OK ' + msg_num + " for mesg: " + msg )
	elif (main_cmd == 'ledoff' ):
		#####---- should insert Led OFF calling code HERE -----#####
		conn.send( 'OK ' + msg_num + " for mesg: " + msg )
	else:
		conn.send( 'FAIL ' + msg_num + ' : Unknown Command' )


class RequestHandler( StreamRequestHandler ):
    def handle( self ):
        print '- Connection from : ', self.client_address
        conn = self.request
        while True:
            msg = conn.recv(1024)
            if not msg:
                conn.close()
                print '! Disconnected from : ', self.client_address
                break
            print '- [Client] ', msg
            processMsg( conn, msg )			# conn.send( msg )

if __name__ == '__main__':
	if ( len(sys.argv) != 2 ):
		print "- Usage : python {} <port>".format( sys.argv[0] )
		print "* Exiting..."
	else:
		gPort = int( sys.argv[1] )			# to integer
		server = ThreadingTCPServer(('', gPort), RequestHandler)
		print '- Listening on port ', gPort
		server.serve_forever()

# End of File
