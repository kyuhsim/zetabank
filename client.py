#!/usr/bin/env python
'''
	Zeta robot protocol between main_board and controller_board.
	Client Program : running on main_board
	(using Python 2.7) 

	History:
		20180529 kyuhsim - created.
'''

import sys
import socket
from threading import Thread

### configurations that can be changed
gHost = "192.168.10.111"	# "localhost"			# "192.168.0.123"
gPort = 9006


### global variables
gSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 	# create socket (SOCK_STREAM means TCP socket) 
gCache = { 'ClientProtocolVersion' : '0.1', 
		'ServerProtocolVersion' : 'Unknown', 
		'ServerIP' : 'localhost', 
		'ServerPort' : 9006,
		'ServerStatus' : 'Unknown', 
		'ClientStatus' : 'Begin',
		'MsgNum' : 0,
	}


def recv_msg(sock):
	MsgRecv = ''
	while True:
		try:
			MsgRecv = gSock.recv(1024)
			if not MsgRecv:
				break
			print "\t\t<--- [Server] {}".format( MsgRecv )			# print( data.decode() )
		except:
			print "* recv() exception"

def client_init():
	gCache[ 'ClientProtocolVersion' ] = '0.1'
	gCache[ 'MsgNum' ] = 0

def connect_socket(host, port):
	gSock.connect((host, port))
	print "- Connected to " + host + "," + str(port) + "."

	gThrd = Thread( target=recv_msg, args=(gSock,))
	gThrd.daemon = True
	gThrd.start()

	gCache[ 'ServerStatus' ] = 'Connected'

def process_msg():
	msg_data = ''

	while msg_data != ":/quit": 
		msg_data = raw_input("- Input : ")
		
		list_msg_data = msg_data.split()						# make a list separated by space
		gCache[ 'MsgNum' ] = gCache[ 'MsgNum' ] + 1				# increment MsgNum

		list_msg_data[1:1] = [ str( gCache[ 'MsgNum' ] ) ]		# insert message number after command
		send_msg_data = " ".join( list_msg_data )				# make string from list with spaces
		try:
			# send data
			gSock.sendall(send_msg_data + "\n")		# space inserted

		finally:
			print "- Sent: {}".format( send_msg_data )

	gSock.close()

if __name__ == '__main__':
	if ( len(sys.argv) != 3 ):
		print "- Usage : python {} <target_host_ip> <port>".format( sys.argv[0] )
		print "* Exiting..."
	else:
		gHost = sys.argv[1]
		gPort = int( sys.argv[2] )			# to integer
		print "- Protocol Client : \n- Commands : \n\tPING, PONG, GetStatus, GetNumPOI, GetPOI, MoveTo (PIO#), Rotate (left/right degree#), LEDON, LEDOFF"
		client_init()
		connect_socket(gHost, gPort)
		process_msg()

# End of File
